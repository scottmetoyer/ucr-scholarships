@extends('layouts.search')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Search Results</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-12">
          <a href="/" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back to Search</a>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
              <thead>
                  <tr>
                      <th>Scholarship</th>
                      <th>Deadline</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  <tr class="odd gradeX">
                      <td><p><a href="/detail">Alumni Association Scholarship Fund</a></p>
                        <div class="label label-warning">Needs-based</div> <div class="label label-success">Undergraduate</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="even gradeC">
                      <td><p><a href="/detail">Lai Yung Huey Memorial Scholarship</p>
                        <div class="label label-warning">Needs-based</div> <div class="label label-success">Undergraduate</div> <div class="label label-danger">Approaching deadline</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="odd gradeA">
                      <td><p><a href="/detail">Arthur L. Littleworth Endowed Scholarship Fund</p>
                        <div class="label label-warning">Needs-based</div> <div class="label label-success">Undergraduate</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="even gradeA">
                      <td><p><a href="/detail">Chancellor's &amp; Regent's Scholarship Fund</p>
                        <div class="label label-danger">Approaching deadline</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="odd gradeA">
                      <td><p><a href="/detail">Charles S. Hoyt Memorial Scholarship Fund</p>
                        <div class="label label-warning">Needs-based</div> <div class="label label-success">Undergraduate</div> <div class="label label-danger">Approaching deadline</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="even gradeA">
                      <td><p><a href="/detail">Colladay Swaney Family Endowed Scholarship Fund</p>
                        <div class="label label-warning">Needs-based</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="odd gradeA">
                      <td><p><a href="/detail">Dr. Thomas Halsey's Chancellor's Scholarship</p>
                        <div class="label label-success">Undergraduate</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="even gradeA">
                      <td><p><a href="/detail">Katrina B. Heinrich-Steinberg Re-Entry Scholarship Endowed Fund</p>
                        <div class="label label-warning">Needs-based</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="odd gradeA">
                      <td><p><a href="/detail">Scott and Jennifer Alden, Father and Daughter Scholarship Fund</p>
                        <div class="label label-warning">Needs-based</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
                  <tr class="even gradeA">
                      <td><p><a href="/detail">Gift Planning Committee Chancellor's Scholarship</p>
                        <div class="label label-success">Undergraduate</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>

                  <tr class="odd gradeA">
                      <td><p><a href="/detail">Frederick E. Gartzke Memorial Endowed Scholarship</p>
                         <div class="label label-success">Undergraduate</div>
                      </td>
                      <td>06/01/2016</td>
                      <td>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-bookmark"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-share"></i></button>
                        <button type="button" class="btn btn-default btn-link"><i class="fa fa-envelope"></i></button>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
    </div>
  </div>

    <!-- /.row -->
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
            responsive: true,
            searching: false,
            lengthChange: false,
            aoColumnDefs: [
         { 'bSortable': false, 'aTargets': [ 2 ] }
      ]
    });
});
</script>
@endsection
