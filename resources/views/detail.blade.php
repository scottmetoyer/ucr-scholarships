@extends('layouts.search')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Alumni Association Scholarship Fund</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                          Details
                      </div>
                        <div class="panel-body">
                          <p>Scholarships for non-traditional UCR students who have experienced a gap in their education of more than five years. Although no particular age range is required, the scholarship is intended to support student who will enter the work force of an extended period, typically at least 15 years after completing their education In the past; Osher Reentry Scholarship recipients have typically ranged in age from the mid-20s to the mid-50s</p>
                          <p><strong>Amount:</strong> Varies</p>
                        </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading">
                          Criteria
                      </div>
                      <div class="panel-body">
                        <p><strong>Minimum GPA:</strong> 2.50</p>
                        <p><strong>Need-based:</strong> Yes</p>
                        <p><strong>Residency:</strong> California resident</p>
                        <p><strong>Enrollment status:</strong></p>
                          <p>Full-time<br>
                          3/4 time<br>
                          1/2 time</p>
                        <p><strong>Academic level:</strong></p>
                      <p>Freshman<br>
                      Sophomore<br>
                      Junior<br>
                      Senior<br></p>
                      <p><strong>Location:</strong></p>
                      <p>Downtown Riverside<br>
                      Polytechnic<br>
                      Tempe<br>
                      West<br></p>
  <p><strong>College:</strong> New College of Interdisciplinary Arts and Sciences</p>
<p><strong>Additional criteria:</strong></p>
<p>Must be an adult reentry student.</p>

<p>Must show evidence of having had their undergraduate degree pursuit at a 2 or 4 year institution interrupted for a gap of 5 years or more.</p>

<p>Although no particular age range is required, the scholarship is intended to support student who will enter the work force of an extended period, typically at least 15 years after completing their education in the past; Osher Reentry Scholarship recipients have typically ranged in age from the mid-20s to the mid-50s.</p>

<p>Must be receiving first baccalaureate degree enrolled in any degree program at ASU</p>

<p>Must be in good standing</p>

<p>Students receiving tuition waivers from employers or other sources would not be eligible for the scholarship.</p>

<p>Must complete a 300-word essay on how this scholarship would advance your educational and career goals.</p>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading">
                          Contact info
                      </div>
                      <div class="panel-body">
                        <address>
                        <a href="http://ucr.edu">New College of Interdisciplinary Arts and Sciences</a><br>
Dr. Vincent Waldron<br>
951-543-6634<br>
https://newcollege.ucr.edu/college-scholarships<br>
Vincew@ucr.edu<br>
</address>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading">
                          Application information
                      </div>
                      <div class="panel-body">
                        <p><strong>Deadline:</strong></p>
<p>Friday, April 15, 2016</p>
<p><a href="/">Click here to apply</a></p>
                      </div>
                    </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
@endsection
