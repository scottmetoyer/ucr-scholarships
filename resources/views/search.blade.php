@extends('layouts.search')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Scholarship Search</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Search Filters
                </div>
                <div class="panel-body">
                  <div class="row hidden-xs" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                    <h4>Instructions</h4>
                    <p>Use the filters below to limit your search results to scholarships that match your status; if you find that your search returns no scholarships try using less filters. This search only shows scholarships with deadlines that have not passed. If you are signed in, you will have the option to add scholarships to your <a href="#">bookmarked scholarships</a> directly from the search results, and your search criteria will be saved.</p>
                  </div>
                  </div>
                    <form role="form">
                      <div class="row">
                        <div class="col-lg-4">
                              <div class="form-group">
                                  <label>Estimated GPA</label>
                                  <select class="form-control form-select" id="edit-gpa" name="gpa">
                                    <option value="All" selected="selected">- Any -</option>
                                    <option value="1">4.00</option><option value="2">3.75</option>
                                    <option value="3">3.50</option><option value="4">3.25</option>
                                    <option value="5">3.00</option><option value="6">2.75</option>
                                    <option value="7">2.50</option><option value="8">2.25</option>
                                    <option value="9">2.00</option>
                                  </select>
                                  <p class="help-block">Your anticipated academic level</p>
                              </div>
                              <div class="form-group">
                                  <label>Enrollment Status</label>
                                <select class="form-control form-select" id="edit-acad-load" name="acad_load">
                                  <option value="All" selected="selected">- Any -</option>
                                  <option value="53">Full-time</option>
                                  <option value="54">3/4 time</option>
                                  <option value="55">1/2 time</option>
                                  <option value="56">Less than 1/2 time</option>
                                </select>
                                  <p class="help-block">Your UCR, transfer, or high school GPA</p>
                              </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-4">
                          <div class="form-group">
                              <label>Academic Level (current or incoming)</label>
                              <select class="form-control form-select" id="edit-acad-level" name="acad_level">
                                <option value="All" selected="selected">- Any -</option>
                                <option value="19">Incoming freshman</option>
                                <option value="2560">Incoming transfer (undergraduate)</option>
                                <option value="20">Freshman</option>
                                <option value="21">Sophomore</option>
                                <option value="22">Junior</option>
                                <option value="23">Senior</option>
                                <option value="24">Post Baccalaureate</option>
                                <option value="25">Graduate</option><option value="26">Law</option>
                              </select>
                              <p class="help-block">As determined by the Registrar's Office</p>
                          </div>
                          <div class="form-group">
                              <label>Residency</label>
                              <select class="form-control form-select" id="edit-residency" name="residency">
                                <option value="All" selected="selected">- Any -</option>
                                <option value="57">California resident</option>
                                <option value="58">Non-California resident</option>
                              </select>
                              <p class="help-block">Current or anticipated enrollment</p>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-12">
                          <div class="panel panel-warning">
                            <div class="panel-heading">
                              <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" class="">Additional filters</a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="true">
                            <div class="panel-body">
                                <p>The following are <strong>highly limiting filters</strong>; consider removing them if no results are returned.</p>

                                <div class="col-lg-6">
                                  <div class="form-group">
                                      <label>Location</label>
                                      <select class="form-control" id="location" name="location">
                                          <option value="All">- Any -</option>
                                          <option>Riverside</option>
                                          <option>Southern California</option>
                                          <option>University of California</option>
                                          <option>International</option>
                                      </select>
                                      <p class="help-block">Campus your major is based in</p>
                                    </div>

                                    <div class="form-group">
                                        <label>College</label>
                                        <select class="form-control" id="college" name="college">
                                            <option>- Any -</option>
                                            <option>Bournes College of Engineering (BCOE)</option>
                                            <option>College of Humanities, Arts, &amp; Social Sciences (CHASS)</option>
                                            <option>College of Natural &amp; Agricultural Sciences (CNAS)</option>
                                            <option>School of Business Administration (SoBA)</option>
                                            <option>Graduate School of Education (GSOE)</option>
                                            <option>School of Medicine</option>
                                            <option>School of Public Policy</option>
                                        </select>
                                        <p class="help-block">Limit to scholarships from your college</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Division</label>
                                        <select class="form-control" id="division" name="division">
                                            <option>- Any -</option>
                                            <option>Division of Agriculture and Natural Resources</option>
                                            <option>Division of Life Sciences</option>
                                            <option>Graduate Division</option>
                                            <option>Division of Physical and Mathematical Sciences</option>
                                            <option>Division of Undergraduate Education</option>
                                        </select>
                                        <p class="help-block">Limit to scholarships from your division</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Major</label>
                                        <select class="form-control">
                                            <option>- Any -</option>
                                        </select>
                                        <p class="help-block">Limit to specific majors</p>
                                    </div>

                                    <div class="form-group">
                                    <label>Keywords</label>
                                    <input class="form-control">
                                    <p class="help-block">Limit to results containing these keywords</p>
                                </div>
                                </div>
                            </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <button type="button" onclick="window.location = '/search-results'" class="btn btn-primary">Search</button>
                          <button type="reset" class="btn btn-default">Reset filters</button>
                        </div>
                      </div>
                      <!-- /.row (nested) -->

                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection
